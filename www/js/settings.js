// Normal Event Listener
document.getElementById('toggleVibration').addEventListener('click', toggleVibration);
document.getElementById('toggleTheme').addEventListener('click', toggleTheme);
document.getElementById('toggleSavingPhoto').addEventListener('click', toggleSavingPhoto);
document.getElementById('saveChatID').addEventListener('click', saveChatID);
document.getElementById('authSave').addEventListener('click', authSave);
document.getElementById('apiSave').addEventListener('click', apiSave);
document.getElementById('apiView').addEventListener('click', apiView);
document.getElementById('serverSave').addEventListener('click', serverSave);

// Clear Data Event Listeners
document.getElementById('clearUsername').addEventListener('click', clearUsername);
document.getElementById('clearPeerJSServer').addEventListener('click', clearPeerJSServer);
document.getElementById('clearDataMap').addEventListener('click', clearDataMap);
document.getElementById('clearDataClicker').addEventListener('click', clearDataClicker);
document.getElementById('clearShortcuts').addEventListener('click', clearShortcuts);
document.getElementById('resetMapFollowPointDefault').addEventListener('click', resetMapFollowPointDefault);


// Hidden Dev Mode
var cnt=32;
function devModeEnable() {
    cnt=parseInt(cnt)-parseInt(1);
    document.getElementById("numberClicks").innerHTML = cnt;
    if(cnt===0) {
        localStorage.setItem("settingTheme", 2);
        var data = {
            message: 'Vous êtes désormais un développeur :).',
            timeout: 3000
        };
        var snackbarContainer = document.querySelector('#snackbar');
        snackbarContainer.MaterialSnackbar.showSnackbar(data);
        loadTheme();
    }
}


// On charge les parametre pour bien mettre les checkbox dans le bon etat
var settingLoad = localStorage.getItem('settingVibration');
if(settingLoad == 1) {
    document.getElementById('toggleVibration').checked = 'checked';
} 
var settingLoad = localStorage.getItem('settingTheme');
if(settingLoad == 1) {
    document.getElementById('toggleTheme').checked = 'checked';
} 
var settingLoadSave = localStorage.getItem('settingSavingPhoto');
if(settingLoadSave == 1) {
    document.getElementById('toggleSavingPhoto').checked = 'checked';
} 
var settingChatID = localStorage.getItem('settingChatID');
if(settingChatID != null) {
    document.getElementById('chatID').value = settingChatID;
}

/* On met les parramètres des API */
var apiOWM = localStorage.getItem('apiOWM');
if(apiOWM != null) {
    document.getElementById('apiOWM').value = apiOWM;
}
var apiMB = localStorage.getItem('apiMB');
if(apiMB != null) {
    document.getElementById('apiMB').value = apiMB;
}


/* On met les settings de l'Auth dans les boites */
var authJeux = localStorage.getItem('authJeux');
if(authJeux == 1) {
    document.getElementById('authJeuxB').checked = 'checked';
}
var authNotes = localStorage.getItem('authNotes');
if(authNotes == 1) {
    document.getElementById('authNotesB').checked = 'checked';
}
var authMap = localStorage.getItem('authMap');
if(authMap == 1) {
    document.getElementById('authMapB').checked = 'checked';
}
var authSettings = localStorage.getItem('authSettings');
if(authSettings == 1) {
    document.getElementById('authSettingsB').checked = 'checked';
}
var authWeather = localStorage.getItem('authWeather');
if(authWeather == 1) {
    document.getElementById('authWeatherB').checked = 'checked';
}
var authChat = localStorage.getItem('authChat');
if(authChat == 1) {
    document.getElementById('authChatB').checked = 'checked';
}

/* On met les settings PeerJS Server dans les boites */
var serverIP = localStorage.getItem('ipServer');
if(serverIP != null) {
    document.getElementById('serverIP').value = serverIP;
}
var serverPort = localStorage.getItem('portServer');
if(serverPort != null) {
    document.getElementById('serverPort').value = serverPort;
}
var serverPath = localStorage.getItem('pathServer');
if(serverPath != null) {
    document.getElementById('serverPath').value = serverPath;
}

function apiSave() {
    var valueBoxapiOWM = document.getElementById('apiOWM').value;
    localStorage.setItem("apiOWM", valueBoxapiOWM);
    
    var valueBoxapiMB = document.getElementById('apiMB').value;
    localStorage.setItem("apiMB", valueBoxapiMB);

    var data = {
        message: 'Clés API enregistrées.',
        timeout: 1000
    };

    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}
function apiView() {
    var api1 = document.getElementById('apiOWM');
    var api2 = document.getElementById('apiMB');
    if(api1.type == "password") {
        api1.type = "text";
        api2.type = "text";
        document.querySelector('#apiView span').innerHTML = "Hide API keys";
    } else {
        api1.type = "password";
        api2.type = "password";
        document.querySelector('#apiView span').innerHTML = "View API keys";
    }
}
function toggleTheme() {
    var setting = localStorage.getItem('settingTheme');
    if(setting == null || setting == 0 || setting == 2) {
        localStorage.setItem("settingTheme", 1);
        var data = {
            message: 'Theme Light activé.',
            timeout: 1000
        };
    } else {
        localStorage.setItem("settingTheme", 0);
        var data = {
            message: 'Theme Dark activé.',
            timeout: 1000
        };
    }
    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
    loadTheme();
}
function clearUsername() {
    document.getElementById('chatID').value = "";
    localStorage.removeItem("settingChatID");
    var data = {
        message: 'Chat ID remis à 0',
        timeout: 1000
    };
    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}
function clearPeerJSServer() {
    document.getElementById('serverIP').value = "";
    document.getElementById('serverPort').value = "";
    document.getElementById('serverPath').value = "";

    localStorage.removeItem("ipServer");
    localStorage.removeItem("portServer");
    localStorage.removeItem("pathServer");

    var data = {
        message: 'Server PeerJS remis à 0',
        timeout: 1000
    };
    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}
function clearShortcuts() {
    localStorage.setItem("shortcuts", "");

    var snackbarContainer = document.querySelector('#snackbar');
    var data = {
        message: 'Données effacées avec succès.',
        timeout: 1000
    };
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}
function clearDataClicker() {
    localStorage.setItem("ClickerButton", 0);
    localStorage.setItem("ClickerToggle", 0);
    localStorage.setItem("ClickerCheckBox", 0);

    var snackbarContainer = document.querySelector('#snackbar');
    var data = {
        message: 'Données effacées avec succès.',
        timeout: 1000
    };
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}
function resetMapFollowPointDefault() {
    localStorage.removeItem("latStored");
    localStorage.removeItem("longStored");

    var snackbarContainer = document.querySelector('#snackbar');
    var data = {
        message: 'Données effacées avec succès.',
        timeout: 1000
    };
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function saveChatID() {
    var setting = localStorage.getItem('settingChatID');
    console.log("Old Chat ID: " + setting);
    var valueBox = document.getElementById('chatID').value;
    // La Regex
    let pattern = /[a-z0-9]{4,30}/i;
    let result = valueBox.match(pattern);

    if(result == valueBox) {
        console.log("New Chat ID: " + valueBox);
        localStorage.setItem("settingChatID", valueBox);
        var data = {
            message: 'Chat ID enregistré: ' + valueBox,
            timeout: 1000
        };
    } else {
        console.log('bad regex');
        var data = {
            message: 'Abandon, seul les lettres de A à Z non accentuées et les chiffres sont autorisés. de 4 à 30 caractères.',
            timeout: 5000
        };
    }
    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function authSave() {
    var jeux = document.getElementById('authJeux').classList;
    var chat = document.getElementById('authChat').classList;
    var notes = document.getElementById('authNotes').classList;
    var map = document.getElementById('authMap').classList;
    var weather = document.getElementById('authWeather').classList;
    var settings = document.getElementById('authSettings').classList;

    if(jeux.contains('is-checked')) {
        localStorage.setItem('authJeux', 1);
    } else {
        localStorage.setItem('authJeux', 0);
    }

    if(chat.contains('is-checked')) {
        localStorage.setItem('authChat', 1);
    } else {
        localStorage.setItem('authChat', 0);
    }

    if(notes.contains('is-checked')) {
        localStorage.setItem('authNotes', 1);
    } else {
        localStorage.setItem('authNotes', 0);
    }

    if(settings.contains('is-checked')) {
        localStorage.setItem('authSettings', 1);
    } else {
        localStorage.setItem('authSettings', 0);
    }

    if(weather.contains('is-checked')) {
        localStorage.setItem('authWeather', 1);
    } else {
        localStorage.setItem('authWeather', 0);
    }

    if(map.contains('is-checked')) {
        localStorage.setItem('authMap', 1);
    } else {
        localStorage.setItem('authMap', 0);
    }

    var data = {
        message: 'Paramètres sauvegardés.',
        timeout: 2000
    };

    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);

}
function serverSave() {
    // Get value and remove all spaces
    var ipaddr = (document.getElementById('serverIP').value).replace(/\s+/g,'');
    var ipport = (document.getElementById('serverPort').value).replace(/\s+/g,'');
    var ippath = (document.getElementById('serverPath').value).replace(/\s+/g,'');
    
    let patternIP = /(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)/i;
    let patternPort = /[0-9]{1,6}/i;
    let patternPath = /\/[a-z]+/i;
    var resultRegexIP = ipaddr.match(patternIP);
    var resultRegexPort = ipport.match(patternPort);
    var resultRegexPath = ippath.match(patternPath);

    if(ipaddr != "" && ipport != "" && ippath != "") {
        if(resultRegexIP[0] == ipaddr) {
            if(resultRegexPort == ipport) {
                if(resultRegexPath == ippath) {
                    localStorage.setItem('ipServer', ipaddr);
                    localStorage.setItem('portServer', ipport);
                    localStorage.setItem('pathServer', ippath);

                    var data = {
                        message: 'Paramètres du serveur PeerJS sauvegardés.',
                        timeout: 5000
                    }; 
                } else {
                    var data = {
                        message: 'Le path n\'est pas au bon format.',
                        timeout: 5000
                    };
                }
            } else {
                var data = {
                    message: 'Le port n\'est pas au bon format.',
                    timeout: 5000
                };
            }
        } else {
            var data = {
                message: 'L\'addresse IPv4 n\'est pas au bon format.',
                timeout: 5000
            };
        }
    } else {
        var data = {
            message: 'Un des champs est vide.',
            timeout: 5000
        };
    }
    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function toggleVibration() {
    var setting = localStorage.getItem('settingVibration');
    if(setting == null || setting == 0) {
        localStorage.setItem("settingVibration", 1);
        var data = {
            message: 'Vibrations desactivée.',
            timeout: 1000
        };
    } else {
        localStorage.setItem("settingVibration", 0);
        var data = {
            message: 'Vibrations activée.',
            timeout: 1000
        };
    }
    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}


function toggleSavingPhoto() {
    var setting = localStorage.getItem('settingSavingPhoto');
    if(setting == null || setting == 0) {
        localStorage.setItem("settingSavingPhoto", 1);
        var data = {
            message: 'Enregistrement des photos désactivé.',
            timeout: 1000
        };
    } else {
        localStorage.setItem("settingSavingPhoto", 0);
        var data = {
            message: 'Enregistrement des photos activé.',
            timeout: 1000
        };
    }
    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function clearDataMap() {
    // This works on all devices/browsers, and uses IndexedDBShim as a final fallback 
    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
    // Open (or create) the database
    var opendb = indexedDB.open("PointMapLists", 1);

    opendb.onupgradeneeded = function() {
        var db = opendb.result;
        var store = db.createObjectStore("objectstorage", {keyPath: "id"});
        var index = store.createIndex("PointsIndex", "name");
    };
    opendb.onsuccess = function() {
        // Start a new transaction
        var db = opendb.result;
        var tx = db.transaction("objectstorage", "readwrite");
        var store = tx.objectStore("objectstorage");
            var index = store.index("PointsIndex");

        var allRecords = store.getAll();
        allRecords.onsuccess = function() {
            // console.log(allRecords.result);
            allRecords.result.forEach(addAllPoints);
        };
        function addAllPoints(item) {
            store.delete(item['id']);
            console.log('deleted id'+item['id'] + ' ' + item['latitude'] + "::"+ item['longitude']);
        }
        // Close the db when the transaction is done
        tx.oncomplete = function() { db.close(); };
    }

    var data = {
        message: 'Waypoints supprimés avec succès.',
        timeout: 1000
    };

    var snackbarContainer = document.querySelector('#snackbar');
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}