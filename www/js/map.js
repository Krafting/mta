var dialog = document.querySelector('dialog');
var showDialogButton = document.querySelector('#show-dialog');
showDialogButton.addEventListener('click', function() { dialog.showModal(); });
dialog.querySelector('.close').addEventListener('click', function() { dialog.close(); });

const compassCircle = document.querySelector(".compass-circle");
const myPoint = document.querySelector(".my-point");

function init() {
    navigator.geolocation.watchPosition(locationHandler);
    window.addEventListener("deviceorientationabsolute", handler, true);
}
function setFollowPoint(lat, long) {
    localStorage.setItem('latStored', lat);
    localStorage.setItem('longStored', long);
    console.log(lat, long)
}
function handler(e) {
    compass = e.webkitCompassHeading || Math.abs(e.alpha - 360);
    compassCircle.style.transform = `translate(-50%, -50%) rotate(${-compass}deg)`;

    // ±15 degree
    if ( (pointDegree < Math.abs(compass) && pointDegree + 15 > Math.abs(compass)) ||
            pointDegree > Math.abs(compass + 15) || pointDegree < Math.abs(compass)) {
        myPoint.style.opacity = 0;
    } else if (pointDegree) {
        myPoint.style.opacity = 1;
    }
}

let pointDegree;

function locationHandler(position) {
    const { latitude, longitude } = position.coords;
    latStore = localStorage.getItem('latStored');
    longStore = localStorage.getItem('longStored');

    if(latStore == null || longStore == null) {
        var latToFollow = 44.836315200186576;
        var longToFollow = 3.0440951705129806;
    } else {
        var latToFollow = latStore;
        var longToFollow = longStore;
    }
    pointDegree = calcDegreeToPoint(latitude, longitude, latToFollow, longToFollow);
    if (pointDegree < 0) {
        pointDegree = pointDegree + 360;
    }
}

function calcDegreeToPoint(latitude, longitude, followLat, followLong) {
    // Qibla geolocation
    const point = {
        lat: followLat,
        lng: followLong
    };
    const phiK = (point.lat * Math.PI) / 180.0;
    const lambdaK = (point.lng * Math.PI) / 180.0;
    const phi = (latitude * Math.PI) / 180.0;
    const lambda = (longitude * Math.PI) / 180.0;
    const psi =
    (180.0 / Math.PI) *
    Math.atan2(
      Math.sin(lambdaK - lambda),
      Math.cos(phi) * Math.tan(phiK) -
        Math.sin(phi) * Math.cos(lambdaK - lambda)
    );
    return Math.round(psi);
}

init();

document.getElementById('followPos').addEventListener('click', followPos);
document.getElementById('gigaChatte').addEventListener('click', localiseME);
document.getElementById('stopFollowPos').addEventListener('click', stopFollowPos);

var autoconnect = getUrlParam("x");
var autoconnect2 = getUrlParam("y");

var markers = []

function createMarker(coords) {
    var id;
    if (markers.length < 1) id = 1
    else id = markers[markers.length - 1]._id + 1
    var popupContent ='<button style="width: 100%" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" onclick="clearMarker(' + id + ')">Delete Point</button><br><br>'+
    '<button style="width: 100%; background: #00989f;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" onclick="setFollowPoint(' + coords[0] + ', '+ coords[1] + ')">Follow Point</button>' +    
    '<br/><br/><a class="mdl-button mdl-js-button mdl-js-ripple-effect" href="weather.html?x='+ coords[0] +'&y='+ coords[1] + '">Open Weather</a>';
    myMarker = L.marker(coords, {
        draggable: false
    });
    myMarker._id = id;
    var myPopup = myMarker.bindPopup(popupContent, {
        closeButton: true
    });
    map.addLayer(myMarker);
    markers.push(myMarker);
}

function addPoint() {
    var lat = document.getElementById('latitude').value;
    var long = document.getElementById('longitude').value;

    var coords = [lat, long]; 

    var id
    if (markers.length < 1) id = 1
    else id = markers[markers.length - 1]._id + 1
    var popupContent ='<button style="width: 100%" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" onclick="clearMarker(' + id + ')">Delete Point</button><br/><br/>' +
        '<button style="width: 100%; background: #00989f;" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" onclick="setFollowPoint(' + lat + ', '+ long + ')">Follow Point</button>' +
        '<br/><br/><a class="mdl-button mdl-js-button mdl-js-ripple-effect" href="weather.html?x='+ lat +'&y='+ long + '">Open Weather</a>';
    myMarker = L.marker(coords, {
        draggable: false
    });
    myMarker._id = id
    var myPopup = myMarker.bindPopup(popupContent, {
        closeButton: false
    });
    map.addLayer(myMarker);
    markers.push(myMarker);

    // This works on all devices/browsers, and uses IndexedDBShim as a final fallback 
    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
    // Open (or create) the database
    var opendb2 = indexedDB.open("PointMapLists", 1);

    opendb2.onsuccess = function() {
        // Start a new transaction
        var db = opendb2.result;
        var tx = db.transaction("objectstorage", "readwrite");
        var store = tx.objectStore("objectstorage");
        var index = store.index("PointsIndex");

        store.put({id: id, latitude: lat, longitude: long});

        // Close the db when the transaction is done
        tx.oncomplete = function() { db.close(); };
    }
    var dialog = document.querySelector('dialog');
    dialog.close();
}

function clearMarker(id) {
    var new_markers = []
    markers.forEach(function(marker) {
        if (marker._id == id) map.removeLayer(marker)
        else new_markers.push(marker)
    })
    markers = new_markers

    // This works on all devices/browsers, and uses IndexedDBShim as a final fallback 
    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
    // Open (or create) the database
    var opendb = indexedDB.open("PointMapLists", 1);

    opendb.onupgradeneeded = function() {
        var db = opendb.result;
        var store = db.createObjectStore("objectstorage", {keyPath: "id", autoIncrement: true});
        var index = store.createIndex("PointsIndex", "name");
    };
    opendb.onsuccess = function() {
        // Start a new transaction
        var db = opendb.result;
        var tx = db.transaction("objectstorage", "readwrite");
        var store = tx.objectStore("objectstorage");
        var index = store.index("PointsIndex");

        var allRecords = store.getAll();
        allRecords.onsuccess = function() {
            // console.log(allRecords.result);
            allRecords.result.forEach(addAllPoints);
        };
        function addAllPoints(item) {
            if(item['id'] == id) {
                console.log('deleted'+id);
                console.log('deleted'+item['id']);
                store.delete(id);
            }
        }
        // Close the db when the transaction is done
        tx.oncomplete = function() { db.close(); };
    }
}

if (autoconnect && autoconnect2) {
    console.log(autoconnect);
    console.log(autoconnect2);
    var map = L.map('map').setView([autoconnect, autoconnect2], 18);
    createMarker([autoconnect, autoconnect2]);
} else {
    var map = L.map('map').setView([47.143808, -1.694294], 15);
    createMarker([47.143808, -1.694294]);
}

var apiMB = localStorage.getItem('apiMB');
if(apiMB == null || apiMB == "") {
    apiMB = "pk.eyJ1IjoiamVqZWhlajY0OCIsImEiOiJja3g1dzR0NHMxNXA0MzBueHR5cmd2N2lsIn0.mLK5MVNikEq5Vtn8R0zm1Q";
}

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 19,
    id: 'mapbox/streets-v11',
    accessToken: apiMB,
    tileSize: 512,
    zoomOffset: -1
}).addTo(map);
function stopFollowPos() {
    document.getElementById('followPos').style.display = 'block';
    document.getElementById('stopFollowPos').style.display = 'none';
    clearInterval(xFollowPosition);
}
function followPos() {
    document.getElementById('followPos').style.display = 'none';
    document.getElementById('stopFollowPos').style.display = 'block';
    xFollowPosition = setInterval(function() {    
        var onSuccess = function(position) {
            map.flyTo([position.coords.latitude,position.coords.longitude], 19);
            var circle = L.circle([position.coords.latitude,position.coords.longitude], {
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0.5,
                radius: 1.5
            }).addTo(map);
        };
        function onError(error) {
            alert('code: '    + error.code    + '\n' +
                'message: ' + error.message + '\n');
        }
        navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true });
        console.log('Following');
    }, 1500);
}

function onMapClick(e) {
    console.log("You clicked the map at " + e.latlng);
    var lat = document.getElementById('latitude').value = e.latlng.lat;
    var long = document.getElementById('longitude').value = e.latlng.lng;

    var circle = L.circle([e.latlng.lat, e.latlng.lng], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.4,
        radius: 30
    }).addTo(map);

    vibrateFunc([250]);
}
map.on('click', onMapClick);

function getUrlParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null) { return null; } else { 
        return results[1]; 
    }
};

function localiseME() {
    var onSuccess = function(position) {
        map.flyTo([position.coords.latitude,position.coords.longitude], 19);
        var circle = L.circle([position.coords.latitude,position.coords.longitude], {
            color: 'blue',
            fillColor: '#30f',
            fillOpacity: 0.4,
            radius: 5
        }).addTo(map);

        createMarker([position.coords.latitude,position.coords.longitude]);
    };
    function onError(error) {
        alert('code: '    + error.code    + '\n' +
            'message: ' + error.message + '\n');
    }
    navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true });
}

document.getElementById("addPoint").addEventListener("click", addPoint);
function fetchAllPoint() {
    // This works on all devices/browsers, and uses IndexedDBShim as a final fallback 
    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
    // Open (or create) the database
    var opendb = indexedDB.open("PointMapLists", 1);

    opendb.onupgradeneeded = function() {
        var db = opendb.result;
        var store = db.createObjectStore("objectstorage", {keyPath: "id"});
        var index = store.createIndex("PointsIndex", "name");
    };
    opendb.onsuccess = function() {
        // Start a new transaction
        var db = opendb.result;
        var tx = db.transaction("objectstorage", "readwrite");
        var store = tx.objectStore("objectstorage");
            var index = store.index("PointsIndex");

        var allRecords = store.getAll();
        allRecords.onsuccess = function() {
            // console.log(allRecords.result);
            allRecords.result.forEach(addAllPoints);
        };
        function addAllPoints(item) {
            createMarker([item['latitude'], item['longitude']]);
            console.log(item['latitude'] + "::"+ item['longitude']);

        }
        // Close the db when the transaction is done
        tx.oncomplete = function() { db.close(); };
    }
}
fetchAllPoint();



// Code pour le speedometer
var onSuccess = function(position) {
    var speed = (convertPosSpeed(position.coords.speed)).toFixed(2); 
    document.getElementById('textSpeed').innerHTML = speed + " km/h";
    if(speed <= 30) {
        document.getElementById('circleSpeed').style.background = "hsl(134, 85%, 66%)";
    } else if(speed <= 60) {
        document.getElementById('circleSpeed').style.background = "hsl(44, 85%, 66%)";
    } else if (speed <= 90) {
        document.getElementById('circleSpeed').style.background = "hsl(359, 85%, 66%)";
    } else if (speed <= 100) {
        document.getElementById('circleSpeed').style.background = "hsl(314, 85%, 66%)";
    } else if (speed > 100) {
        document.getElementById('circleSpeed').style.background = "hsl(269, 85%, 66%)";
    }
};
function onError(error) {
    alert('code: '    + error.code    + '\n' +
        'message: ' + error.message + '\n');
}
navigator.geolocation.watchPosition(onSuccess, onError, { enableHighAccuracy: true });