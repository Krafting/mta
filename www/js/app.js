/* Header, nommer la page */
document.getElementById("page-title-layout").innerHTML = document.title;

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function escapeHtml(text) {
	var map = {
	  "'": '&#039;'
	};
	return text.replace(/[']/g, function(m) { console.log(m); return map[m]; });
}


function decodeHtmlCharCodes(str) { 
	return str.replace(/(&#(\d+);)/g, function(match, capture, charCode) {
	  return String.fromCharCode(charCode);
	});
}

function sendChip(idChip, idContent, Message) {
	var alerts = document.getElementById(idChip);
	var alertContent = document.getElementById(idContent);
	alerts.style.display = "block";
	alerts.classList.add("opacClass");
	alertContent.innerHTML = Message;
	setTimeout(function() {
		alerts.style.display = "none";
		alerts.classList.remove("opacClass");
	}, 2000);
	console.log("P2P CHAT: " + Message);
}

function addZero(t) {
	if (t < 10)
		t = "0" + t;
	return t;
};

function debug() {
    window['counter'] = 0;
    var snackbarContainer = document.querySelector('#demo-toast-example');
    var data = {message: 'Example Message # ' + ++counter};
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function vibrateFunc(pattern = 500) {
	var settingVibrate = localStorage.getItem('settingVibration');
	if(settingVibrate != 1) {
		window.navigator.vibrate(pattern);
	}
}

/* GESTURE */
if(document.title != "Map" && document.title != "Two-Sides") {
	var hammertime = new Hammer(document);
	// listen to events...
	hammertime.on("panright", function(ev) {
		console.log("Swiped")
		document.querySelector('.mdl-layout__drawer-button').click();
	});
}
if(document.title != "Home Page") {
	document.addEventListener("deviceready", onDeviceReadyAllApp);

	// RETIRER CETTE LIGNE QUAND ON DEV DANS LE NAVIGATEUR
	// document.querySelector('body').style.display = "none";

	function onDeviceReadyAllApp() {
		if(document.title == "Chat Rooms") {
			var authChat = localStorage.getItem('authChat');
			if(authChat == 1) {
				Fingerprint.show({
					description: "Enter credentials to access Chat Rooms"
				}, successCallback, errorCallback);
			} else {
				document.querySelector('body').style.display = "block";
			}
		} else if(document.title == "Notes") {
			var authNotes = localStorage.getItem('authNotes');
			if(authNotes == 1) {
				Fingerprint.show({
					description: "Enter credentials to access Notes"
				}, successCallback, errorCallback);
			} else {
				document.querySelector('body').style.display = "block";
			}
		} else if(document.title == "Settings") {
			var authSettings = localStorage.getItem('authSettings');
			if(authSettings == 1) {
				Fingerprint.show({
					description: "Enter credentials to access Settings"
				}, successCallback, errorCallback);
			} else {
				document.querySelector('body').style.display = "block";
			}
		} else if(document.title == "Jeux" || document.title == "Reflexes" || document.title == "Two-Sides") {
			var authJeux = localStorage.getItem('authJeux');
			if(authJeux == 1) {
				Fingerprint.show({
					description: "Enter credentials to access Jeux"
				}, successCallback, errorCallback);
			} else {
				document.querySelector('body').style.display = "block";
			}
		} else if(document.title == "Weather") {
			var authWeather = localStorage.getItem('authWeather');
			if(authWeather == 1) {
				Fingerprint.show({
					description: "Enter credentials to access Weather"
				}, successCallback, errorCallback);
			} else {
				document.querySelector('body').style.display = "block";
			}
		} else if(document.title == "Map") {
			var authMap = localStorage.getItem('authMap');
			if(authMap == 1) {
				Fingerprint.show({
					description: "Enter credentials to access Map"
				}, successCallback, errorCallback);
			} else {
				document.querySelector('body').style.display = "block";
			}
		} else {
			document.querySelector('body').style.display = "block";
		}

		function successCallback() {
			console.log("Authentication successful");
			document.querySelector('body').style.display = "block";
		}

		function errorCallback(error) {
			console.log("Authentication invalid " + error.message);
			location.replace('index.html');
		}
	}
}

function loadTheme() {
	var settingTheme = localStorage.getItem('settingTheme');
	if(settingTheme == 1) {
		document.querySelector('html').classList.add('bg-light');
	} else if(settingTheme == 2) {
		document.querySelector('html').classList.add('bg-dev');
	} else {
		document.querySelector('html').classList.remove('bg-light');
		document.querySelector('html').classList.remove('bg-dev');
	}
}
loadTheme();


function convertPosSpeed(speed) {
	if(speed == null) {
		speed = 0;
	} else {
		speed = parseFloat(speed) * 3.6;
	}
	return speed;
}