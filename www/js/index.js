/* LISTENERS */
document.addEventListener("deviceready", onDeviceReady);

/* FUNCTIONS */
async function onDeviceReady() {
    /* LOADING SCREEN */
	console.log('doc ready');
    document.getElementById("loading").style.display = "none";
    document.getElementById("app").style.display = "block";
}
var join;

var lastPeer = window.localStorage.getItem('lastpeer');
if (lastPeer != null) {
    join = "?connect="+lastPeer;
} else {
    join = "";
}
var arrayListLinks =  [
    { "lien": "autre.html", "nom": "Autre"},
    { "lien": "map.html", "nom": "Map"},
    { "lien": "jeux.html", "nom": "Jeux"},
    { "lien": "tools.html", "nom": "Outils"},
    { "lien": "chat.html", "nom": "Chat Rooms"},
    { "lien": "notes.html", "nom": "Notes"},
    { "lien": "help.html", "nom": "Aide"},
    { "lien": "receive.html", "nom": "Créer une Room"},
    { "lien": "send.html", "nom": "Rejoindre une Room"},
    { "lien": "send.html"+join, "nom": "Dernière Session"},
    { "lien": "weather.html", "nom": "Météo"},
    { "lien": "reflexe.html", "nom": "Jeu: Reflexes"},
    { "lien": "hangman.html", "nom": "Jeu: Hangman"},
    { "lien": "juste-prix.html", "nom": "Jeu: Juste-prix"},
    { "lien": "two-sides.html", "nom": "Jeu: Two-Sides"},
    { "lien": "leboule.html", "nom": "Jeu: La Boule"}
];
var selectShortcut = document.getElementById('idShortcut');
for (var i = 0; i < arrayListLinks.length; i++){
    var obj = arrayListLinks[i];
    selectShortcut.insertAdjacentHTML('beforeend', '<option value="'+ i +'">' + obj['nom'] + '</option>');
}

function refreshShortcuts() {
    var listShortcut = localStorage.getItem('shortcuts');
    if(listShortcut == null) {
        listShortcut = "";
    }
    var result = listShortcut.split("|");
    var shortcutList = document.getElementById('shortcutList');

    shortcutList.innerHTML = "";
    
    if(listShortcut.length < 1 || listShortcut.length == null) {
        shortcutList.insertAdjacentHTML('beforeend', '\
        <div class="defaultSC">\
            <i class="material-icons noSCIcon" role="presentation">add</i>\
            <p>Il n\'y a aucun Shortcut pour le moment, ajoutez en un dès maintenant!</p>\
        </div>');
    };
    
    for (var i = 0; i < result.length-1; i++) {
        var obj = result[i];
        shortcutList.insertAdjacentHTML('beforeend', '\
            <div class="mdl-cell mdl-cell--6-col"style="text-align: center;">\
            <a href="'+ arrayListLinks[obj]['lien'] +'" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bigButton">\
                '+ arrayListLinks[obj]['nom'] +'\
            </a>\
            </div>');
    }

}
refreshShortcuts();

document.getElementById("addShortcut").addEventListener("click", addShortcut);

var dialog = document.querySelector('dialog');
var showDialogButton = document.querySelector('#show-shortcutsAdd');
if (! dialog.showModal) { dialogPolyfill.registerDialog(dialog); }
showDialogButton.addEventListener('click', function() { dialog.showModal(); });
dialog.querySelector('.close').addEventListener('click', function() { dialog.close(); });

function addShortcut() {
    var selectShortcut = document.getElementById('idShortcut');
    var listShortcut = localStorage.getItem('shortcuts');
    if(listShortcut == null) {
        listShortcut = "";
    }
    var newValue = listShortcut + selectShortcut.value + "|";
    var listShortcut = localStorage.setItem('shortcuts', newValue);
    refreshShortcuts();
    dialog.close();
}

