function showData(data) {
    if (data == 'daily') {
        document.getElementById('previsionTableTable').style.display = "table";
        document.getElementById('previsionTableHourlyTable').style.display = "none";
    } else if (data == 'hourly') {
        document.getElementById('previsionTableTable').style.display = "none";
        document.getElementById('previsionTableHourlyTable').style.display = "table";
    }
}
var dialog = document.querySelector('dialog');
var showDialogButton = document.querySelector('#show-dialog-city');

showDialogButton.addEventListener('click', function() { dialog.showModal(); });
dialog.querySelector('.close').addEventListener('click', function() { dialog.close(); });

document.getElementById('showCityMeteo').addEventListener('click', showMeteoCity);

var apiOWM = localStorage.getItem('apiOWM');
if(apiOWM == null || apiOWM == "") {
    apiOWM = "ad3bf63f5413a4b28664e26fa2ec379e";
}

function showMeteoCity() {
    ville = document.getElementById('villeCity').value;
    location.replace("weather.html?ville="+ville);
}
function  toTextualDescription(degree){
    if (degree>337.5) return 'N';
    if (degree>292.5) return 'NW';
    if(degree>247.5) return 'W';
    if(degree>202.5) return 'SW';
    if(degree>157.5) return 'S';
    if(degree>122.5) return 'SE';
    if(degree>67.5) return 'E';
    if(degree>22.5){return 'NE';}
    return 'N';
}

var latitudeMapCord = getUrlParam("x");
var longitudeMapCord = getUrlParam("y");
var villeMapCord = getUrlParam("ville");
var novilleMapCord = getUrlParam("nocity");

if (latitudeMapCord && longitudeMapCord) {
    console.log(latitudeMapCord);
    console.log(longitudeMapCord);
    fetchData(latitudeMapCord, longitudeMapCord, 0, 0);
} else if (villeMapCord) {
    console.log(villeMapCord);

    // On cherche la latitude et la longitude de la ville
    const urlCity = "https://api.openweathermap.org/data/2.5/weather?q="+villeMapCord+"&limit=1&appid="+apiOWM;				
    fetch(urlCity).then((response)=>{  
        return response.json(); 
    }).then(data=>{
        if(data["cod"] == 404) {
            location.replace('weather.html?nocity=yesindeed');
        } else {
            latitudeMapCordCity = data["coord"]["lat"];
            longitudeMapCordCity = data["coord"]["lon"];
            fetchData(latitudeMapCordCity, longitudeMapCordCity, 0, 0);
        }
    });
} else {
    if(novilleMapCord) {
        sendChip("alertMessage","contentAlertMessage", "Ville non trouvée.");
    }
    var onSuccess = function(position) {
        fetchData(position.coords.latitude,position.coords.longitude, position.coords.altitude, position.coords.speed);
    };
    function onError(error) {
        alert('code: '    + error.code    + '\n' +
            'message: ' + error.message + '\n');
    }
    navigator.geolocation.watchPosition(onSuccess, onError, { enableHighAccuracy: true });
}

function fetchData(xx, yy, alt, speed) {
    speed = convertPosSpeed(speed);
    document.getElementById('lienMap').href = "map.html?x="+ xx + "&y=" + yy;
    document.getElementById('lat').innerHTML = parseFloat(xx).toFixed(12);
    document.getElementById('long').innerHTML = parseFloat(yy).toFixed(12);
    if(alt == null || alt == 0) {
        console.log('Pas d\'altitude');
        document.getElementById('altitudeblock').style.display = "none";
    }
    if(speed == null || speed < 0.05) {
        console.log('Pas de speed');
        document.getElementById('speedblock').style.display = "none";
    } else {
        document.getElementById('speedblock').style.display = "table-cell";
    }
    savedSpeed = localStorage.getItem('MaxSpeed2');
    if (speed > savedSpeed || savedSpeed == null) {
        localStorage.setItem('MaxSpeed2', speed);
        document.getElementById('speedmax').innerHTML = speed + " km/h"
    }
    savedSpeed2 = localStorage.getItem('MaxSpeed2');
    document.getElementById('speedmax').innerHTML = parseFloat(savedSpeed2).toFixed(2) + " km/h"
    document.getElementById('alt').innerHTML = Math.floor(alt) + " m";
    document.getElementById('speed').innerHTML = parseFloat(speed).toFixed(2) + " km/h";

    // ON PREND LES INFOS SUPPLEMENTAIRE ?lat={lat}&lon={lon}&appid={API key}
    const url = "https://api.openweathermap.org/geo/1.0/reverse?lat="+xx+"&lon="+yy+"&limit=1&appid="+apiOWM;
    const urlActual = "https://api.openweathermap.org/data/2.5/weather?lat="+xx+"&lon="+yy+"&limit=1&appid="+apiOWM;
    const urlPrevision = "https://api.openweathermap.org/data/2.5/onecall?lat="+xx+"&lon="+yy+"&appid="+apiOWM;
    console.log(urlPrevision);
    fetch(url).then((response)=>{
        return response.json(); 
    }).then(data=>{
        document.getElementById('pays').innerHTML = data[0]["country"];
        document.getElementById('ville1').innerHTML = data[0]["name"];
        if(data[0]["state"] == null) {
            document.getElementById('state').style.display = "none";
        }
        document.getElementById('etat').innerHTML = data[0]["state"];
        var i = 0;
        document.getElementById('tableListNom').innerHTML = "";
        for (let key in data[0]["local_names"]) {
            i = i + 1;
            let value = data[0]["local_names"][key];
            document.getElementById('tableListNom').innerHTML = document.getElementById('tableListNom').innerHTML + '<tr><td class="leftFocused">Nom de la ville ['+ key +'] <br/> <p id="pays" class="leftData">'+value+'</p></td></tr>';
        }
    })
    fetch(urlActual).then((response)=>{  
        return response.json(); 
    }).then(data=>{
        document.getElementById('imageCond').src = "https://openweathermap.org/img/wn/" + data["weather"][0]["icon"] + "@2x.png";
        document.getElementById('conditions').innerHTML = data["weather"][0]["main"];
        document.getElementById('temp').innerHTML = (parseFloat(data["main"]["temp"])-273).toFixed(2) + " °C";
        document.getElementById('ress').innerHTML = (parseFloat(data["main"]["feels_like"])-273).toFixed(2)  + " °C";
        document.getElementById('tempmin').innerHTML = (parseFloat(data["main"]["temp_min"])-273).toFixed(2) + " °C";
        document.getElementById('tempmaxi').innerHTML = (parseFloat(data["main"]["temp_max"])-273).toFixed(2) + " °C";
        document.getElementById('press').innerHTML = data["main"]["pressure"] + " hPa";
        document.getElementById('hum').innerHTML = data["main"]["humidity"] + " %";
        document.getElementById('wind').innerHTML = data["wind"]["speed"] + " m/s";
        document.getElementById('winddir').innerHTML = toTextualDescription(data["wind"]["deg"]);
        document.getElementById('visibility').innerHTML = data["visibility"] + " m";
        var d = new Date(data["sys"]["sunrise"]*1000);
        document.getElementById('rise').innerHTML = d.getHours() + "h " + d.getMinutes() + "m " + d.getSeconds()+ "s";
        var d = new Date(data["sys"]["sunset"]*1000);
        document.getElementById('set').innerHTML = d.getHours() + "h " + d.getMinutes() + "m " + d.getSeconds()+ "s";
    })

    fetch(urlPrevision).then((response)=>{  
        return response.json(); 
    }).then(data=>{
        document.getElementById('previsionTable').innerHTML = "";
        document.getElementById('previsionTableHourly').innerHTML = "";
        for (let key in data["daily"]) {

            let value = data["daily"][key];

            var d = new Date(value["dt"]*1000);
            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            var date = days[d.getDay()] + " " + d.getDate() + " " + monthNames[d.getMonth()];


            var d = new Date(value["sunrise"]*1000);
            var sunrise = d.getHours() + "h " + d.getMinutes() + "m " + d.getSeconds()+ "s";
            var d = new Date(value["sunset"]*1000);
            var sunset = d.getHours() + "h " + d.getMinutes() + "m " + d.getSeconds()+ "s";

            let content = "<tr><td style='text-align: left;padding: 10px;'>\
                    <b style='font-size: 18px;'>" + date + "</b>\
                    <br><b>" + value['weather'][0]['main'] + "</b> " + value['weather'][0]['description'] + "\
                    <br><b>Sunrise:</b> " + sunrise + "\
                    <br><b>Sunset:</b> " + sunset + "\
                    <br><b>Humidité:</b> " + value['humidity'] + "\ %\
                    <br><b>Pression:</b> " + value['pressure']  + "\ hPa\
                </td>\
                <td style='padding: 10px;'>\
                    <b>Temp. Jour:</b> " + (parseFloat(value["temp"]["day"])-273).toFixed(2) + " °C\
                    <br><b>Temp. Nuit:</b> " + (parseFloat(value["temp"]["night"])-273).toFixed(2) + " °C\
                    <br><b>Temp. Min.:</b> " + (parseFloat(value["temp"]["min"])-273).toFixed(2) + " °C\
                    <br><b>Temp. Max.:</b> " + (parseFloat(value["temp"]["max"])-273).toFixed(2) + "\ °C\
                    <br><b>Vitesse du vent:</b> " + value['wind_speed'] + " m/s\
                    <br><b>Direction du vent:</b> " + toTextualDescription(value["wind_deg"]) + "\
                </td></tr>";
            
            document.getElementById('previsionTable').insertAdjacentHTML("beforeend", content);
        }
        var i = 0;
        for (let key in data["hourly"]) {
            i= i + 1
            document.getElementById('meteaoHourly').innerHTML = "Météo des " + i + " prochaines heures"
            let value = data["hourly"][key];

            var d = new Date(value["dt"]*1000);
            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            var date = days[d.getDay()] + " " + d.getDate() + " " + monthNames[d.getMonth()] + " (" + d.getHours() + "h)";


            var d = new Date(value["sunrise"]*1000);
            var sunrise = d.getHours() + "h " + d.getMinutes() + "m " + d.getSeconds()+ "s";
            var d = new Date(value["sunset"]*1000);
            var sunset = d.getHours() + "h " + d.getMinutes() + "m " + d.getSeconds()+ "s";

            let content = "<tr><td style='text-align: left;padding: 10px;'>\
                    <b style='font-size: 18px;'>" + date + "</b>\
                    <br><b>" + value['weather'][0]['main'] + "</b> " + value['weather'][0]['description'] + "\
                    <br><b>Humidité:</b> " + value['humidity'] + "\ %\
                    <br><b>Pression:</b> " + value['pressure']  + "\ hPa\
                </td>\
                <td style='padding: 10px;'>\
                    <b>Temperature:</b> " + (parseFloat(value["temp"])-273).toFixed(2) + " °C\
                    <br><b>Ressenti:</b> " + (parseFloat(value["feels_like"])-273).toFixed(2) + " °C\
                    <br><b>Vitesse du vent:</b> " + value['wind_speed'] + " m/s\
                    <br><b>Direction du vent:</b> " + toTextualDescription(value["wind_deg"]) + "\
                </td></tr>";
            
            document.getElementById('previsionTableHourly').insertAdjacentHTML("beforeend", content);
        }
    })
}


function getUrlParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return null;
    else
        return results[1];
};

document.getElementById('clearVitesseMax').addEventListener('click', clearVitesseMax);
function clearVitesseMax() {
    localStorage.removeItem("MaxSpeed2");
    document.getElementById('speedmax').innerHTML = "0 km/h"
    sendChip("alertMessage","contentAlertMessage", 'Données effacées avec succès.')
}