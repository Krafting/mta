document.querySelector('#netStat').addEventListener("click", showNetStat);
document.getElementById("qrCodeScanner").addEventListener("click", scan);

function showNetStat() {
    var snackbarContainer = document.querySelector('#demo-snackbar-example');
    var status = showStatus();
    var data = {
		message: 'Network Status: ' + status,
		timeout: 2000
    };
    snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

function showStatus() {
	var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'Aucune connexion';

	return states[networkState];
}



function scan() {
	console.log("scan");
	cordova.plugins.barcodeScanner.scan(scanResult, scanError);
}

function scanResult(result) {
	console.log("scanResult");
	navigator.notification.alert(
			"Text: " + result.text + "\n" +
			"Format: " + result.format + "\n" +
			"Cancelled: " + result.cancelled,
			null,
			"Scan Result",
			'OK'
	);
}

function scanError(error) {
	console.log("scanError");
	navigator.notification.alert(
			"Error: " + error,
			null,
			"Scan Error",
			'OK'
	);
}


document.getElementById('speedMS').addEventListener('keyup', msTOkmh);
document.getElementById('speedKMH').addEventListener('keyup', kmhTOms);
function msTOkmh() {
	var ms = document.getElementById('speedMS').value;
	kmhValue = ms * 3.6;
	if(!isNaN(kmhValue)) {
		document.getElementById('speedKMH').value = kmhValue
	}
}
function kmhTOms() {
	var kmh = document.getElementById('speedKMH').value;
	msValue = kmh / 3.6;
	if(!isNaN(msValue)) {
		document.getElementById('speedMS').value = msValue
	}
}