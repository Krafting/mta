function closeMap() {
	var map = document.getElementById('frameMap');
	var buttonmap = document.getElementById('buttonCloseMap');
	map.style.display = "none";
	map.src = "";
	buttonmap.style.display = "none";
}
function showPage(xx, yy, page) {
	var map = document.getElementById('frameMap');
	var buttonmap = document.getElementById('buttonCloseMap');
	map.src = "./"+ page +".html?x=" + xx + "&y=" + yy;
	map.style.display = "block";
	buttonmap.style.display = "block";
}
function toggleFileOptions() {
	var classBox = document.getElementById('fileOptions');
	var buttonRot = document.getElementById('showFileOption');
	if(buttonRot.classList.contains("rotatedButton")) {
		classBox.style.bottom = "0px";
		buttonRot.classList.remove('rotatedButton');
	} else {
		classBox.style.bottom = "80px";
		buttonRot.classList.add('rotatedButton');
	}
}
function setCisco(ciscoSetting, recuSend, clicked, id = null) {
	document.getElementById(recuSend).value=ciscoSetting;
	document.getElementById(clicked).click();
	if(id != null) {
		var recu = document.getElementById('ciscoRecu').value
		var repondu = document.getElementById('ciscoSend').value
		console.log(recu)
		console.log(repondu)
		document.getElementById(id).innerHTML = recu;
	} else {
		var dialog2 = document.querySelector('#cisco-dialog');
		dialog2.close();
	}
}
function checkWin(recu, repondu) {
	var win;
	if(recu == repondu) { win =  "Il y a égaliter"; }
	else if(recu == "Papier" && repondu == "Cisco") { win = 'Vous avez gagné'; }
	else if(recu == "Papier" && repondu == "Marco") { win = 'Vous avez perdu'; }
	else if(recu == "Marco" && repondu == "Papier") { win =  'Vous avez gagné'; }
	else if(recu == "Marco" && repondu == "Cisco") { win = 'Vous avez perdu'; }
	else if(recu == "Cisco" && repondu == "Marco") { win = 'Vous avez gagné'; }
	else if(recu == "Cisco" && repondu == "Papier") { win = 'Vous avez perdu'; }
	else { win = "Erreur."; }
	return win;
}

function copy(id) {
	navigator.clipboard.writeText(document.getElementById(id).innerHTML);
	var range = document.createRange();
	range.selectNode(document.getElementById(id));
	window.getSelection().removeAllRanges(); // clear current selection
	window.getSelection().addRange(range); // to select text
	document.execCommand("copy");
	window.getSelection().removeAllRanges();// to deselect
	sendChip("alertMessage","contentAlertMessage", "ID Copier dans le presse-papier.");
}
function showQRCode(id) {
	console.log(id);
	var random = Math.floor(Math.random()*10000);
	var viewQR = document.getElementById("viewQR");
	viewQR.style.display = "block";

	var qrDiv = document.getElementById("qrcanv");
	qrDiv.innerHTML = "";
	var text = document.getElementById(id).innerHTML;
	var qrc = new QRCode(qrDiv, text);
	viewQR.scrollIntoView();
	sendChip("alertMessage","contentAlertMessage", "QR Code affiché.");
}
function loadImage(id) {
	var modal = document.getElementById('modalImage');
	var img = document.getElementById('showImage'+id);
	var modalImg = document.getElementById('imageInModal');
	console.log('Image Zoomed'); 
	modal.style.display = 'block';
	modalImg.src = img.src;
};
function closeImage() {
	var modal = document.getElementById('modalImage');
	modal.style.display = 'none';
};


// Le scanner de QR Code
function scanResult(result) {
	var recvIdInput = document.getElementById("receiver-id");
	recvIdInput.value= result.text;
	var sendIdInput = document.getElementById("connect-button");
	sendIdInput.click();
	if(result.cancelled === true) {
		sendIdInput.innerHTML = '<i class="material-icons" role="presentation">login</i>';
	}
}
function scanError(error) {
	sendChip("alertMessage","contentAlertMessage", "Error: " + error);
	document.getElementById('connect-button').innerHTML = '<i class="material-icons" role="presentation">login</i>';
}
function scan() { 
	cordova.plugins.barcodeScanner.scan(scanResult, scanError);
}

// LE DIALOG DE SUPPRESSION DES MSG
var dialog = document.querySelector('dialog');
var showDialogButton = document.querySelector('#clearMsgsButtonPopUp');
showDialogButton.addEventListener('click', function() {
	dialog.showModal();
});
dialog.querySelector('.close-delete').addEventListener('click', function() {
	dialog.close();
});

// LE DIALOG DE MARCO CISCO PAPIER
var dialog2 = document.querySelector('#cisco-dialog');
var showDialogButton = document.querySelector('#ciscoPopup');
showDialogButton.addEventListener('click', function() {
	dialog2.showModal();
});
dialog2.querySelector('.close-cisco').addEventListener('click', function() {
	dialog2.close();
});

// LE DIALOG DES NOTES
var dialog3 = document.querySelector('#note-dialog');
var showDialogButton = document.querySelector('#showSendNoteButton');
showDialogButton.addEventListener('click', function() {
	dialog3.showModal();
});
dialog3.querySelector('.close-note').addEventListener('click', function() {
	dialog3.close();
});