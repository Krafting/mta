function makeDiv(div) {
    // vary size for fun
    var divsize = ((Math.random()*100) + 50).toFixed();
    var color = Math.round(0xffffff * Math.random()).toString(16);
    while(color.length < 6) {
        color = "0" + color;
    }
    color = '#' + color
    $newdiv = $('<div id="div-'+div+'"/>').css({
        'width':divsize+'px',
        'height':divsize+'px',
        'background-color': color
    });

    var thisDiv = document.getElementById(div)

    // make position sensitive to size and document's width
    var posx = (Math.random() * ($(thisDiv).width() - divsize)).toFixed();
    var posy = (Math.random() * ($(thisDiv).height() - divsize)).toFixed();

    $newdiv.css({
        'position':'relative',
        'left':posx+'px',
        'top':posy+'px',
        'display':'none'
    }).appendTo( '#' +div ).fadeIn(100);
};


var aleatoire = Math.random() * (parseInt(3) - parseInt(0.5) +1) + parseInt(0.5);

xTimer = setInterval(function() {
    makeDiv("right");
    makeDiv("left");    
    document.getElementById('div-left').addEventListener('click', player1);
    document.getElementById('div-right').addEventListener('click', player2);    
    clearInterval(xTimer);
}, 1000*aleatoire);

function player1() {
    document.getElementById('flexContent').innerHTML = '<h1 style="text-align:center;width:100%">Le joueur bleu gagne!</h1>\
    <a href="?replay" style="background:linear-gradient(267deg,#6006fa,#ff0101)" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bigButton">Rejouer\
    <span class="mdl-button__ripple-container"><span class="mdl-ripple is-animating" style="width: 3604.72px; height: 3604.72px; transform: translate(-50%, -50%) translate(947px, 18px);">\
        </span></span></a>';
    document.getElementById('flexContent').style.background = "blue";
    document.getElementById('flexContent').style.display = "block";
}
function player2() {
    document.getElementById('flexContent').innerHTML = '<h1 style="text-align:center;width:100%">Le joueur rouge gagne!</h1>\
    <a href="?replay" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bigButton" data-upgraded=",MaterialButton,MaterialRipple">Rejouer\
    <span class="mdl-button__ripple-container"><span class="mdl-ripple is-animating" style="width: 3604.72px; height: 3604.72px; transform: translate(-50%, -50%) translate(947px, 18px);">\
        </span></span></a>';
    document.getElementById('flexContent').style.background = "red";
    document.getElementById('flexContent').style.display = "block";
}