var yes = [ "Ouais!", "Il semblerais que oui", "Certes", "Yep", "ui", "Faut croire que oui" ];
var no = ["Non, désolé","Nuyp", "nn pd", "Malheureusement, non", "J'pense pas", "C'est mort"];
var redo = ["J'ai pas compris repete s'te plait", "Les esprits sont perturbés, réessaie"];
document.getElementById('btnBouleMagique').addEventListener('click', function() {
    document.getElementById('bouleMagique').innerHTML = "";
    var answer = Math.floor(Math.random() * 5);
    if(answer == 0 || answer == 2) {
        choisi = yes[Math.floor(Math.random() * yes.length)];
        color = "rgb(57, 255, 78)";
    } else if(answer == 1 || answer == 3) {
        choisi = no[Math.floor(Math.random() * no.length)];
        color = "rgb(255, 57, 57)";
    } else {
        choisi = redo[Math.floor(Math.random() * redo.length)];
        color = "#0093ff";
    }
    document.getElementById('laBouleImage').classList.add("shake");
    
    var xInter = setInterval(function() {
        document.getElementById('bouleMagique').innerHTML = choisi;
        document.getElementById('bouleMagique').style.color = color;
        document.getElementById('laBouleImage').classList.remove("shake");
        clearInterval(xInter);
    }, 600);
});
