
var manche = localStorage.getItem('roundsSet');
var pseudo = localStorage.getItem('pseudoSet');
var min = localStorage.getItem('minSet');
var max = localStorage.getItem('maxSet');
var gamestylevar = localStorage.getItem('gamestyleSet');

if(pseudo != null) {
    document.getElementById('pseudo').value = pseudo;
}
if(min != null) {
    document.getElementById('min').value = min;
}
if(max != null) {
    document.getElementById('max').value = max;
}
if(manche != null) {
    document.getElementById('manche').value = manche;
}
if(gamestylevar != null) {
    if (gamestylevar == 0) {
        document.querySelector('#gamestyleButton0 span').innerHTML = "<small>[x]</small> Finger-Style";
    } else if(gamestylevar == 1) {
        document.querySelector('#gamestyleButton1 span').innerHTML = "<small>[x]</small> Shake-Style";
    } else { }
    document.getElementById('gamestyle').value = gamestylevar;
}

function gamestyle(id) {
    if (id == 0) {
        document.querySelector('#gamestyleButton0 span').innerHTML = "<small>[x]</small> Finger-Style";
        document.querySelector('#gamestyleButton1 span').innerHTML = "Shake-Style";
    } else if(id == 1) {
        document.querySelector('#gamestyleButton0 span').innerHTML = "Finger-Style";
        document.querySelector('#gamestyleButton1 span').innerHTML = "<small>[x]</small> Shake-Style";
    } else {
        document.querySelector('#gamestyleButton0 span').innerHTML = "Finger-Style";
        document.querySelector('#gamestyleButton1 span').innerHTML = "Shake-Style";
    }
    document.getElementById('gamestyle').value = id;
}
document.addEventListener("deviceready", fetchHighscore, false);
function resetscores() {

	var db = window.sqlitePlugin.openDatabase({name: 'krafting_db.db', location: 'default' }, function (db) {}, function (error) { console.log('Open database ERROR: ' + JSON.stringify(error)); });  
	db.transaction(function(tx) {
		tx.executeSql('DELETE FROM REFLEXEHIGHSCORES');
	});
    fetchHighscore();

	var dialog = document.querySelector('#delete-dialog');
	dialog.close();
}
function fetchHighscore() {
    var db = window.sqlitePlugin.openDatabase({name: 'krafting_db.db', location: 'default' }, function (db) {}, function (error) { console.log('Open database ERROR: ' + JSON.stringify(error)); });  
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS REFLEXEHIGHSCORES (id integer primary key, pseudo text, nbround text, average text, tempsmin text, tempsmax text, gamestyle text)');
        
        // LES HIGH SCORES DE FINGER
        tx.executeSql('SELECT * FROM REFLEXEHIGHSCORES WHERE gamestyle=0 ORDER BY length(average), average ASC', [], function(tx, res) {
            var len = res.rows.length;
            var node = document.getElementById('listHighScoreFingering');
            console.log(len);
            // node.innerHTML = "";
            node.insertAdjacentHTML('afterbegin', '<table style="width:100%; margin-top: 25px;" class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp"><tbody id="fingering"></tbody></table>')
            for (var i = 0; i < len; i++) {
                var id = res.rows.item(i).id;
                var pseudoDB = res.rows.item(i).pseudo;
                var nbroundDB = res.rows.item(i).nbround;
                var averageDB = res.rows.item(i).average;
                var minDB = res.rows.item(i).tempsmin;
                var maxDB = res.rows.item(i).tempsmax;
                var gamestyleDB = "Finger";

                var content = '<tr><td class="mdl-data-table__cell--non-numeric"><b>#'+ (i+1) + '</b> ' + pseudoDB + '<br><b>Average:</b> '+ averageDB +'ms</td>\
                  <td><b>Min:</b> '  + parseInt(minDB).toFixed(2) + 's <br><b>Max:</b> '+ parseInt(maxDB).toFixed(2) +'s</td>\
                  <td><b>Rounds:</b> ' + nbroundDB + '<br><b>Gamestyle:</b> '+ gamestyleDB +'</td>\
                </tr>';
                document.getElementById('fingering').insertAdjacentHTML('beforeend', content);
            }

            if(len < 1) {
                node.innerHTML = '<div style="margin: 25px 20px 0 ;">\
                    <p>Il n\'y a aucun High Score pour le mode de jeu Shake. Jouer au jeu pour montrer votre superiorité!</p>\
                </div>';
            };
        }, function(e) {
            console.log("some error getting");
        });

        // LES HIGH SCORE DE SHAKE
        tx.executeSql('SELECT * FROM REFLEXEHIGHSCORES WHERE gamestyle=1 ORDER BY length(average), average ASC', [], function(tx, res) {
            var len = res.rows.length;
            var node = document.getElementById('listHighScoreShaking');
            console.log(len);
            // node.innerHTML = "";
            node.insertAdjacentHTML('afterbegin', '<table style="width:100%; margin-top: 25px;" class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp"><tbody id="shaking"></tbody></table>')
            for (var i = 0; i < len; i++) {
                var id = res.rows.item(i).id;
                var pseudoDB = res.rows.item(i).pseudo;
                var nbroundDB = res.rows.item(i).nbround;
                var averageDB = res.rows.item(i).average;
                var minDB = res.rows.item(i).tempsmin;
                var maxDB = res.rows.item(i).tempsmax;
                var gamestyleDB = "Shake";

                var content = '<tr><td class="mdl-data-table__cell--non-numeric"><b>#'+ (i+1) + '</b> ' + pseudoDB + '<br><b>Average:</b> '+ averageDB +'ms</td>\
                  <td><b>Min:</b> '  + minDB + 's <br><b>Max:</b> '+ maxDB +'s</td>\
                  <td><b>Rounds:</b> ' + nbroundDB + '<br><b>Gamestyle:</b> '+ gamestyleDB +'</td>\
                </tr>';
                document.getElementById('shaking').insertAdjacentHTML('beforeend', content);
            }

            // node.insertAdjacentHTML('beforeend', '</tbody></table>')
            if(len < 1) {
                node.innerHTML = '<div style="margin: 25px 20px 0 ;">\
                    <p>Il n\'y a aucun High Score pour le mode de jeu Shake. Jouer au jeu pour montrer votre superiorité!</p>\
                </div>';
            };
        }, function(e) {
            console.log("some error getting");
        });
    });
}

document.getElementById("resetButton").addEventListener("click", showDeleteDB);

document.querySelector('#delete-dialog').querySelector('.close-delete').addEventListener('click', function() {
	document.querySelector('#delete-dialog').close();
});
function showDeleteDB() {
	var dialog = document.querySelector('#delete-dialog');
	dialog.showModal();
}

function traitement() {
    var min2,max2,manche2,pseudo2;

    //-----------------------PSEUDO-----------------------
    var pseudo = document.getElementById("pseudo").value;
    var regex_pseudo = /[a-z_0-9]{2,20}/i;
    let result_pseudo = pseudo.match(regex_pseudo);
    if (result_pseudo == pseudo) {
        pseudo2 = true;
    } else {
        alert ("Mauvais pseudo");
        pseudo2 = false; 
    }
    //-------------------------MIN && MAX-----------------
    var min = document.getElementById("min").value;
    var max = document.getElementById("max").value;
    if ( min > 0 && min <= max) {
        min2 = true;
    } else {
        alert ("Mauvais min");
        min2 = false;
    }
    if ( max >= 1 && max >= min) {
        max2 = true;
    } else {
        alert ("Mauvais max");
        max2 = false;
    }

    var manche = document.getElementById("manche").value;
    if ( manche >= 1 && manche <= 50) {
        manche2 = true;
    } else {
        alert ("Mauvaise manche");
        manche2 = false;
    }

    var gamestyle = document.getElementById('gamestyle').value ;

    if (pseudo2 == true && min2 == true && max2 == true && manche2 == true) {
        location.replace('reflexeGame.html?pseudo='+pseudo+'&min='+min+'&max='+max+'&manche='+manche+'&gamestyle='+gamestyle);
    }
}