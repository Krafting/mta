function defaultGame() {

}
function newGame() {
    guesses = 0;
    numberMax = 0;
    if(parseInt(localStorage.getItem('JPnumMax')) > 0 && parseInt(localStorage.getItem('JPguesses')) > 0) {
        numberMax = localStorage.getItem('JPnumMax');
        guesses = localStorage.getItem('JPguesses');
        document.getElementById('nombre').value = numberMax;
        document.getElementById('guesses').value = guesses;
    } else {
        numberMax = 1000;
        guesses = 15;
    }
    number = Math.floor(Math.random()*numberMax);
    console.log(number);
    console.log(guesses);
    maxGuesses = guesses;
    document.querySelector('.guessesLeft span').innerHTML =  guesses + " / " + maxGuesses;
    document.querySelector('.maxnum span').innerHTML =  numberMax;
    document.querySelector('.guessed span').innerHTML = "";
    document.getElementById('input').style.display = "block";
    document.getElementById('buttonss').style.display = "none";
    document.getElementById('hint').innerHTML = "New Game!";
    document.getElementById('hint').style.color = "#0093ff";
    document.getElementById('statusmsg').innerHTML = "New Game!";
}
newGame();

function saveSetting() {
    var numberMaxSave = document.getElementById('nombre').value;
    var guessesSave = document.getElementById('guesses').value;
    if(!isNaN(numberMaxSave) && numberMaxSave != "" && !isNaN(guessesSave) && guessesSave != "") {
        localStorage.setItem('JPnumMax', numberMaxSave);
        localStorage.setItem('JPguesses', guessesSave);
    } else {
        alert('bad settings');
    }
    newGame();
}

document.querySelector('input').addEventListener('change', function() {
    if(guesses <= 0) {
        document.getElementById('hint').innerHTML = "You lost!";
        document.getElementById('hint').style.color = "rgb(255, 57, 57)";
        document.getElementById('statusmsg').innerHTML = "The number was : <b>" + number + "</b>";
        document.getElementById('buttonss').style.display = "flex";
        document.getElementById('input').style.display = "none";
    } else {
        if (!isNaN(this.value)) {
            guesses--;
            document.querySelector('.guessesLeft span').innerHTML =  guesses + " / " + maxGuesses;
            if(this.value > number) {
                document.getElementById('hint').innerHTML = "- Less -";
                document.getElementById('hint').style.color = "rgb(255, 122, 57)";
            } else if(this.value < number) {
                document.getElementById('hint').innerHTML = "+ More +";
                document.getElementById('hint').style.color = "rgb(57, 134, 255)";
            } else if(this.value == number) {
                document.getElementById('hint').innerHTML = "You Won!";
                document.getElementById('hint').style.color = "rgb(57, 255, 78)";
                document.getElementById('statusmsg').innerHTML = "The number was : <b>" + number + "</b>";
                document.getElementById('buttonss').style.display = "flex";
                document.getElementById('input').style.display = "none";
                console.log('WP');
            }
            document.getElementById('statusmsg').innerHTML = "Dernier nombre : <b>" + this.value + "</b>";
            document.querySelector('.guessed span').innerHTML = document.querySelector('.guessed span').innerHTML + " : " + this.value;
            this.value = "";
        } else {
            document.getElementById('hint').innerHTML = "! Numbers only !";
            document.getElementById('hint').style.color = "rgb(165, 57, 255)";
        }
    }
});

document.getElementById('replay').addEventListener('click', newGame);
