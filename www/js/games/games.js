/* LISTENERS */
document.getElementById("clickerButton").addEventListener("click", addToClickerButton);
document.getElementById("clickerToggle").addEventListener("click", addToClickerToggle);
document.getElementById("clickerCheckBox").addEventListener("click", addToClickerCheckBox);
document.getElementById("clickerButton").addEventListener("focus", clickerHoldButton);
document.getElementById("clickerButton").addEventListener("blur", clickerHoldButtonLost);

/* FUNCTIONS */
newValue = +(localStorage.getItem("ClickerButton"));
document.getElementById("clickerButtonText").innerHTML = newValue;
newValue = +(localStorage.getItem("ClickerToggle"));
document.getElementById("clickerToggleText").innerHTML = newValue;
newValue = +(localStorage.getItem("ClickerCheckBox"));
document.getElementById("clickerCheckBoxText").innerHTML = newValue;


button = +(localStorage.getItem("ClickerButton"));
toggle = +(localStorage.getItem("ClickerToggle"));
checkbox = +(localStorage.getItem("ClickerCheckBox"));
if ((toggle < checkbox) && (button < checkbox)) {
    document.getElementById("star1").style.display = "none";
    document.getElementById("star2").style.display = "none";
    document.getElementById("star3").style.display = "inline-block";
}
button = +(localStorage.getItem("ClickerButton"));
toggle = +(localStorage.getItem("ClickerToggle"));
checkbox = +(localStorage.getItem("ClickerCheckBox"));
if ((toggle < button) && (checkbox < button)) {
    document.getElementById("star1").style.display = "inline-block";
    document.getElementById("star2").style.display = "none";
    document.getElementById("star3").style.display = "none";
}
button = +(localStorage.getItem("ClickerButton"));
toggle = +(localStorage.getItem("ClickerToggle"));
checkbox = +(localStorage.getItem("ClickerCheckBox"));
if ((button < toggle) && (checkbox < toggle)) {
    document.getElementById("star1").style.display = "none";
    document.getElementById("star2").style.display = "inline-block";
    document.getElementById("star3").style.display = "none";
}
function clickerHoldButton() {
    // La vibration fun (séparer pour plus de flexibilité)
    holdingButtonVibre = setInterval(function() {
        vibrateFunc([400]);
    }, 400);
    holdingButton = setInterval(function() {
        oldValue = +(localStorage.getItem("ClickerButton"));
        localStorage.setItem("ClickerButton",oldValue+1);
        newValue = +(localStorage.getItem("ClickerButton"));
        document.getElementById("clickerButtonText").innerHTML = newValue;
        button = +(localStorage.getItem("ClickerButton"));
        toggle = +(localStorage.getItem("ClickerToggle"));
        checkbox = +(localStorage.getItem("ClickerCheckBox"));
        if ((toggle < button) && (checkbox < button)) {
            document.getElementById("star1").style.display = "inline-block";
            document.getElementById("star2").style.display = "none";
            document.getElementById("star3").style.display = "none";
        }
    }, 5);
    document.querySelector('#clickerButton span').textContent = "ULTRA POWER!!";
}

function clickerHoldButtonLost() {
    clearInterval(holdingButtonVibre);
    clearInterval(holdingButton);
    document.querySelector('#clickerButton span').textContent = "Click Me!";
}

function addToClickerButton() {
    // La vibration fun
	vibrateFunc([100]);

    oldValue = +(localStorage.getItem("ClickerButton"));
    localStorage.setItem("ClickerButton",oldValue+1);
    newValue = +(localStorage.getItem("ClickerButton"));
    // console.log(localStorage.getItem("ClickerButton"));

    document.getElementById("clickerButtonText").innerHTML = newValue;

    button = +(localStorage.getItem("ClickerButton"));
    toggle = +(localStorage.getItem("ClickerToggle"));
    checkbox = +(localStorage.getItem("ClickerCheckBox"));
    if ((toggle < button) && (checkbox < button)) {
        document.getElementById("star1").style.display = "inline-block";
        document.getElementById("star2").style.display = "none";
        document.getElementById("star3").style.display = "none";
    }
}

function addToClickerToggle() {
    // La vibration fun
	vibrateFunc([250]);
    
    oldValue = +(localStorage.getItem("ClickerToggle"));
    localStorage.setItem("ClickerToggle",oldValue+0.5);
    newValue = +(localStorage.getItem("ClickerToggle"));

    document.getElementById("clickerToggleText").innerHTML = newValue;

    button = +(localStorage.getItem("ClickerButton"));
    toggle = +(localStorage.getItem("ClickerToggle"));
    checkbox = +(localStorage.getItem("ClickerCheckBox"));
    if ((button < toggle) && (checkbox < toggle)) {
        document.getElementById("star1").style.display = "none";
        document.getElementById("star2").style.display = "inline-block";
        document.getElementById("star3").style.display = "none";
    }
}
function addToClickerCheckBox() {
    // La vibration fun
	vibrateFunc([400]);
    
    oldValue = +(localStorage.getItem("ClickerCheckBox"));
    localStorage.setItem("ClickerCheckBox",oldValue+0.5);
    newValue = +(localStorage.getItem("ClickerCheckBox"));
    // console.log(localStorage.getItem("ClickerCheckBox"));

    document.getElementById("clickerCheckBoxText").innerHTML = newValue;

    button = +(localStorage.getItem("ClickerButton"));
    toggle = +(localStorage.getItem("ClickerToggle"));
    checkbox = +(localStorage.getItem("ClickerCheckBox"));
    if ((toggle < checkbox) && (button < checkbox)) {
        document.getElementById("star1").style.display = "none";
        document.getElementById("star2").style.display = "none";
        document.getElementById("star3").style.display = "inline-block";
    }
}


/* D2 */
document.getElementById("1d2").addEventListener("click", d2dice);
function d2dice() {
    document.getElementById("resultDices").innerHTML = "";
	vibrateFunc([50]);
    var valueGet = Math.floor((Math.random() * 2) + 1);
    if(valueGet == 1) {
        document.getElementById("resultDices").innerHTML = "Pile!";
    } else {
        document.getElementById("resultDices").innerHTML = "Face!";
    }
}


/* D4 */
document.getElementById("1d4").addEventListener("click", d4dice);
function d4dice() {
	vibrateFunc([50]);
    document.getElementById("resultDices").innerHTML = Math.floor((Math.random() * 4) + 1);
}


/* d6 */
document.getElementById("1d6").addEventListener("click", d6dice);
function d6dice() {
	vibrateFunc([50]);
    document.getElementById("resultDices").innerHTML = Math.floor((Math.random() * 6) + 1);
}


/* D10 */
document.getElementById("1d10").addEventListener("click", d10dice);
function d10dice() {
	vibrateFunc([50]);
    document.getElementById("resultDices").innerHTML = Math.floor((Math.random() * 10) + 1);
}


/* D16 */
document.getElementById("1d16").addEventListener("click", d16dice);
function d16dice() {
	vibrateFunc([50]);
    document.getElementById("resultDices").innerHTML = Math.floor((Math.random() * 16) + 1);
}


/* D20 */
document.getElementById("1d20").addEventListener("click", d20dice);
function d20dice() {
	vibrateFunc([50]);
    document.getElementById("resultDices").innerHTML = Math.floor((Math.random() * 20) + 1);
}


/* D50 */
document.getElementById("1d50").addEventListener("click", d50dice);
function d50dice() {
	vibrateFunc([50]);
    document.getElementById("resultDices").innerHTML = Math.floor((Math.random() * 50) + 1);
}


/* D10 */
document.getElementById("1d100").addEventListener("click", d100dice);
function d100dice() {
	vibrateFunc([50]);
    document.getElementById("resultDices").innerHTML = Math.floor((Math.random() * 100) + 1);
}


/* D10 */
document.getElementById("1d1000").addEventListener("click", d1000dice);
function d1000dice() {
	vibrateFunc([50]);
    document.getElementById("resultDices").innerHTML = Math.floor((Math.random() * 1000) + 1);
}

