//---------------recuperation--des--variables--de--l'url--------------------
var pseudo = getUrlParam('pseudo');
var min = getUrlParam('min');
var max = getUrlParam('max');
var manche = getUrlParam('manche');
var gamestyle = getUrlParam('gamestyle');
document.addEventListener("deviceready", documentReady, false);

function documentReady() {
    var db = window.sqlitePlugin.openDatabase({name: 'krafting_db.db', location: 'default' }, function (db) {}, function (error) { console.log('Open database ERROR: ' + JSON.stringify(error)); });  
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS REFLEXEHIGHSCORES (id integer primary key, pseudo text, nbround text, average text, tempsmin text, tempsmax text, gamestyle text)');
    }); 
    if(gamestyle == 1) {
        document.getElementById('launchGame').innerHTML = "Secoue ton tel pour commencer!";
        document.getElementById('waitForIt').innerHTML = "Wait for the shake..";
        document.getElementById('gameIsOn').innerHTML = "SHAKE LE!";
        shake.startWatch(onShake, 40, onError);
    } else {
        document.getElementById('zero').addEventListener('click', startgame , false);
        document.getElementById('gameOn').addEventListener('click', timedGame , false);
        document.getElementById('waiting').addEventListener('click', tropTot , false);
    }
}


localStorage.setItem('averageTime', "0");
localStorage.setItem('rounds', manche);
localStorage.setItem('roundsSet', manche);
localStorage.setItem('pseudoSet', pseudo);
localStorage.setItem('minSet', min);
localStorage.setItem('maxSet', max);
localStorage.setItem('gamestyleSet', gamestyle);
function changeSetting() {
    location.replace('reflexe.html');
}

//-----------------------------event listener--------------------------------
var onShake = function () {
    var zeroDiv = document.getElementById('zero');
    var gameOnDiv = document.getElementById('gameOn');
    var waitingDiv = document.getElementById('waiting');
    // Fired when a shake is detected
    if(zeroDiv.style.display == "block") {
        startgame();
    } else if(waitingDiv.style.display == "block") {
        tropTot();
    } else if(gameOnDiv.style.display == "block") {
        timedGame();
    }  else {
        tropTot();
    }
};
  
var onError = function () {
    alert("error")
// Fired when there is an accelerometer error (optional)
};
  

// Start watching for shake gestures and call "onShake"
// with a shake sensitivity of 40 (optional, default 30)
// Stop watching for shake gestures
// shake.stopWatch();
//--------------------------------function------------------------------------

function entierAleatoire(min, max) {
    return Math.random() * (parseInt(max) - parseInt(min) +1) + parseInt(min);
}
function getFullMediaURL(audioFil) {
    return cordova.file.applicationDirectory + audioFil;
}

function playMP3(file) {
    let src = getFullMediaURL(file);
    var myMedia =
    new Media(src,
        function () { },
        function (e) { alert('Media Error: ' + JSON.stringify(e)); }
    );
    myMedia.play();
    myMedia.setVolume('1.0');
}

function tropTot() {
    playMP3('www/audio/felpd.mp3') 
    alert('Félicitation PD! T\'as appuyer trop tôt, on recommence cette manche.');
    // window.location.reload();
    clearInterval(xTimer);
    document.getElementById('stats').style.display = "none";
    document.getElementById('zero').style.display = "block";
    document.getElementById('gameOn').style.display = "none";
    document.getElementById('waiting').style.display = "none";
}
function play(audioFile) {
    var audio = new Audio(audioFile);
    audio.play();
}
function getUrlParam(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    
        if (results == null)
            return null;
                else
            return results[1];
};
//-------------------------compte-a-rebours---------------------------------------
function startgame() {
    document.getElementById('zero').style.display = "none";
    document.getElementById('waiting').style.display = "block";
    document.getElementById('stats').style.display = "none";
    document.getElementById('gameOn').style.display = "none";

    var aleatoire2 = entierAleatoire(min,max);

    xTimer = setInterval(function() {
        localStorage.setItem('appearTime', new Date().getTime());
        vibrateFunc([250]);
        document.getElementById('zero').style.display = "none";
        document.getElementById('gameOn').style.display = "block";
        document.getElementById('waiting').style.display = "none";
        clearInterval(xTimer);
    }, 1000*aleatoire2);
    
}
//--------------------------start-game--------------------------------------------
function timedGame() {
    var timeNow = new Date().getTime();
    var manchesNum = localStorage.getItem('rounds');
    
    if (manchesNum <= 1) {
        document.getElementById('stats').style.display = "block";
        document.getElementById('zero').style.display = "none";
        document.getElementById('gameOn').style.display = "none";
        document.getElementById('waiting').style.display = "none";

        localStorage.setItem('touchedTime', timeNow);
        var statsBlock = document.getElementById('tempsDeLatence');


        var time = (parseInt(localStorage.getItem('touchedTime'))-parseInt(localStorage.getItem('appearTime')));

        var oldAgerageTimeNC = localStorage.getItem('averageTime');
        console.log(oldAgerageTimeNC);
        var newAverageTime = parseInt(oldAgerageTimeNC) + parseInt(time)
        localStorage.setItem('averageTime', newAverageTime);


        var averageTime = (parseInt(localStorage.getItem('averageTime')) / manche).toFixed(2);
        console.log(averageTime);

        var db = window.sqlitePlugin.openDatabase({name: 'krafting_db.db', location: 'default' }, function (db) {}, function (error) { console.log('Open database ERROR: ' + JSON.stringify(error)); });  
        db.transaction(function(tx) {
            tx.executeSql('INSERT INTO REFLEXEHIGHSCORES (pseudo,nbround,average,tempsmin,tempsmax,gamestyle) VALUES (?,?,?,?,?,?)', [pseudo, manche, averageTime, min, max, gamestyle]);
        });

        statsBlock.innerHTML = "<div id='content'><span class='msRound'>Temp Moyen: " + averageTime + "</span> ms</div><br>" +
            statsBlock.innerHTML + 
            "<div id='content'><span class='msRound'>" + time + "</span> ms</div>";

    } else {
        localStorage.setItem('rounds', manchesNum-1);
        localStorage.setItem('touchedTime', timeNow);

        var statsBlock = document.getElementById('tempsDeLatence');

        var time = (parseInt(localStorage.getItem('touchedTime'))-parseInt(localStorage.getItem('appearTime')));
        console.log(time + "TIME")

        var oldAgerageTime = localStorage.getItem('averageTime');
        console.log(oldAgerageTime + "DEFAULT");
        var averageTime = (parseInt(oldAgerageTime) + time)
        console.log(averageTime + "ADDED");
        localStorage.setItem('averageTime', averageTime);

        statsBlock.innerHTML = statsBlock.innerHTML + "<div id='content'><span class='msRound'>" + time + "</span> ms" +"</div>";
        document.getElementById('stats').style.display = "none";
        document.getElementById('zero').style.display = "block";
        document.getElementById('waiting').style.display = "none";
        document.getElementById('gameOn').style.display = "none";
    }
    
}
