document.addEventListener("deviceready", fetchNewData, false);

document.getElementById("saveNote").addEventListener("click", saveNote);
document.getElementById("updateNote").addEventListener("click", updateNote);
document.getElementById("deleteNote").addEventListener("click", deleteNote);
document.getElementById("addLocationEdit").addEventListener("click", addLocationEdit);
document.getElementById("removeLocationEdit").addEventListener("click", removeLocationEdit);
document.getElementById("addLocation").addEventListener("click", addLocation);
document.getElementById("removeLocation").addEventListener("click", removeLocation);

var dialog = document.querySelector('dialog');
var showDialogButton = document.querySelector('#show-dialog');

showDialogButton.addEventListener('click', function() { dialog.showModal(); });
dialog.querySelector('.close').addEventListener('click', function() { dialog.close(); });

document.querySelector('#edit-dialog').querySelector('.close-edit').addEventListener('click', function() {
	document.querySelector('#edit-dialog').close();
});
document.querySelector('#delete-dialog').querySelector('.close-delete').addEventListener('click', function() {
	document.querySelector('#delete-dialog').close();
});

const Editor = toastui.Editor;
const { codeSyntaxHighlight } = Editor.plugin;

const editNoteContent = new Editor({
	el: document.querySelector('#edit-note-content'),
	height: 'auto',
	previewStyle: 'tab',
	initialEditType: 'markdown',
	hideModeSwitch: true,
	placeholder: 'Entrer le texte de la note..',
	plugins: [[codeSyntaxHighlight, { highlighter: Prism }]]
});
const addNoteContent = new Editor({
	el: document.querySelector('#add-note-content'),
	hideModeSwitch: true,
	height: 'auto',
	plugins: [[codeSyntaxHighlight, { highlighter: Prism }]],
	previewStyle: 'tab',
	initialEditType: 'markdown',
	placeholder: 'Entrer le texte de la note..'
});

function fetchNewData() {
	var db = window.sqlitePlugin.openDatabase({name: 'krafting_db.db', location: 'default' }, function (db) {}, function (error) { console.log('Open database ERROR: ' + JSON.stringify(error)); });  
	db.transaction(function(tx) {
	tx.executeSql('CREATE TABLE IF NOT EXISTS NOTESUPDATE (id integer primary key, titre text, contenu text, flextype integer, location text)');
	tx.executeSql('SELECT * FROM NOTESUPDATE', [], function(tx, res) {
		var len = res.rows.length;
		var node = document.getElementById('notes-content');
		console.log(len);

		node.innerHTML = "";
		for (var i = 0; i < len; i++) {
			var flex = res.rows.item(i).flextype;
			if(flex == 1) {
				extended = "1 100";
			} else {
				extended = "0 40";
			}

			text = decodeHtmlCharCodes(res.rows.item(i).contenu);
			// converter = new showdown.Converter();
			// html = converter.makeHtml(text);

			if (res.rows.item(i).location == "") {
				showMap = "";
			} else {
				latlong = res.rows.item(i).location
				lat = latlong.split('::')[0]
				long = latlong.split('::')[1]
				showMap = '<a href="map.html?x='+lat+'&y='+long+'" style="flex: 1 0 33%;box-sizing: border-box;" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><i class="material-icons">explore</i></a>';
			}
			node.insertAdjacentHTML('afterbegin', '\
			<div class="demo-card-square mdl-card mdl-shadow--2dp" style="flex: 1 ' + extended + '%;margin: 5px;min-height: 1px;"> \
				<div class="mdl-card__title mdl-card--expand" style="flex: 0.5;">\
					<h2 class="mdl-card__title-text">' + res.rows.item(i).titre + '</h2>\
				</div>\
				<div id="noteShownContent" class="mdl-card__supporting-text noteShownContent" style="width: auto;padding: 5px 10px;">\
					<div  id="viewer-' +  res.rows.item(i).id + '"></div>\
				</div>\
				<div class="mdl-card__actions mdl-card--border">\
					<div style="display: flex;">\
						<button id="show-dialog--" style="flex: 1 0 33%;" onClick="showNote(' + res.rows.item(i).id + ');" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><i class="material-icons">edit</i></button>\
						<button  style="flex: 1 0 33%;" onClick="showDeleteNote(' + res.rows.item(i).id + ');" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><i class="material-icons">delete</i></button>\
						' + showMap + '\
					</div>\
				</div>\
			</div>');
			const { codeSyntaxHighlight } = toastui.Editor.plugin;		
			const viewer = new toastui.Editor.factory({
				el: document.querySelector('#viewer-'+res.rows.item(i).id),
				viewer: true,
				plugins: [[codeSyntaxHighlight, { highlighter: Prism }]],
				initialValue: text
			});
		}
		if(len < 1) {
			node.insertAdjacentHTML('afterbegin', '\
			<div class="defaultNotes">\
				<i class="material-icons noNoteIcon" role="presentation">note_add</i>\
				<p>Il n\'y a aucune note pour le moment. Créez en une dès maintenant!</p>\
			</div>');
		};
	}, function(e) {
		console.log("some error getting");
	});
	});
}

function removeLocationEdit() {
	document.getElementById('showLocationEdit').style.display = "none";
	document.getElementById('locationFormUpdated').value = "";
}
function addLocationEdit() {
	var onSuccess = function(position) {
		document.getElementById('locationNoteEdit').innerHTML = "<a href='map.html?x=" + position.coords.latitude + "&y=" + position.coords.longitude + "'>[ " + position.coords.latitude + "," + position.coords.longitude + " ] </a>";
		document.getElementById('locationFormUpdated').value = position.coords.latitude + "::" + position.coords.longitude
		document.getElementById('showLocationEdit').style.display = "flex";
	};
	function onError(error) {
		alert('code: ' + error.code + '\n message: ' + error.message + '\n');
	}
	navigator.geolocation.getCurrentPosition(onSuccess, onError);
}
function removeLocation() {
	document.getElementById('showLocation').style.display = "none";
	document.getElementById('locationForm').value = "";
}
function addLocation() {
	var onSuccess = function(position) {
		document.getElementById('locationNote').innerHTML = "<a href='map.html?x=" + position.coords.latitude + "&y=" + position.coords.longitude + "'>[ " + position.coords.latitude + "," + position.coords.longitude + " ] </a>";
		document.getElementById('locationForm').value = position.coords.latitude + "::" + position.coords.longitude
		document.getElementById('showLocation').style.display = "flex";
	};
	function onError(error) {
		alert('code: ' + error.code + '\n message: ' + error.message + '\n');
	}
	navigator.geolocation.getCurrentPosition(onSuccess, onError);
}

function saveNote() { 
	var titre = document.getElementById("titreNote").value;
	var contenu = escapeHtml(addNoteContent.getMarkdown());
	var location = document.getElementById("locationForm").value;

	// var docBox = document.getElementById("extendedNoteLabel");
	// if(docBox.classList.contains("is-checked")) {
	// 	var flexchoice = 1;
	// } else {
	// 	var flexchoice = 0;
	// }
	flexchoice = 1
	var db = window.sqlitePlugin.openDatabase({name: 'krafting_db.db', location: 'default' }, function (db) {}, function (error) { console.log('Open database ERROR: ' + JSON.stringify(error)); });  
	db.transaction(function(tx) {
		tx.executeSql('CREATE TABLE IF NOT EXISTS NOTESUPDATE (id integer primary key, titre text, contenu text, flextype integer, location text)');
		tx.executeSql('INSERT INTO NOTESUPDATE (titre,contenu,flextype, location) VALUES (?,?,?,?)', [titre, contenu, flexchoice, location]);
	});
	// db.close();

	// ON VIDE LES CHAMPS PRECEDEMENT UTILISER
	document.getElementById("titreNote").value = "";
	addNoteContent.setMarkdown("")

	var dialog = document.querySelector('dialog');
	fetchNewData();
	removeLocation();
	sendChip("alertMessage","contentAlertMessage", "Note ajoutée avec succès.");
	dialog.close();
}
function updateNote(id) {
	var id = document.getElementById('numeroNoteUpdated').value;
	var titre = document.getElementById('titreNoteUpdated').value;
	var location = document.getElementById("locationFormUpdated").value;
	var contenu = escapeHtml(editNoteContent.getMarkdown());

	var db = window.sqlitePlugin.openDatabase({name: 'krafting_db.db', location: 'default' }, function (db) {}, function (error) { console.log('Open database ERROR: ' + JSON.stringify(error)); });  
	
	// var docBoxUpdated = document.getElementById("extendedNoteLabelUpdated");
	// if(docBoxUpdated.classList.contains("is-checked")) {
	// 	var flexchoice = 1;
	// } else {
	// 	var flexchoice = 0;
	// }
	flexchoice = 1
	db.transaction(function(tx) {
		var query = "UPDATE NOTESUPDATE SET titre='" + titre + "', contenu='" + contenu + "', flextype='" + flexchoice + "', location='" + location + "' WHERE id=" + id;
		tx.executeSql(query, [], function(tx, res) {});
	});
	
	fetchNewData();
	removeLocationEdit();
	var dialog = document.querySelector('#edit-dialog');
	dialog.close();
	sendChip("alertMessage","contentAlertMessage", "Note modifiée avec succès.");
}
function showNote(id) {
	var idNote = document.getElementById('numeroNoteUpdated');
	idNote.value = id;
	
	var db = window.sqlitePlugin.openDatabase({name: 'krafting_db.db', location: 'default' }, function (db) {}, function (error) { console.log('Open database ERROR: ' + JSON.stringify(error)); });  
	db.transaction(function(tx) {
		var query = "SELECT * FROM NOTESUPDATE WHERE id='" + id + "';";
		tx.executeSql(query, [], function(tx, res) {
			var titreNote = document.getElementById('titreNoteUpdated');
			titreNote.value = res.rows.item(0).titre;

			editNoteContent.setMarkdown(decodeHtmlCharCodes(res.rows.item(0).contenu));
			

			if (res.rows.item(0).location == "") {
				document.getElementById('locationNoteEdit').innerHTML = "";
				document.getElementById('locationFormUpdated').value = "";
				document.getElementById('showLocationEdit').style.display = "none";
			} else {
				latlong = res.rows.item(0).location;
				lat = latlong.split('::')[0];
				long = latlong.split('::')[1];
				document.getElementById('locationNoteEdit').innerHTML = "<a href='map.html?x=" + lat + "&y=" + long + "'>[ " + lat + "," + long + " ] </a>";
				document.getElementById('locationFormUpdated').value = lat + "::" + long
				document.getElementById('showLocationEdit').style.display = "flex";
			}
			var idNote = document.getElementById('numeroNoteUpdated');
			idNote.value = res.rows.item(0).id;

			var docBoxUpdated = document.getElementById("extendedNoteLabelUpdated");
			if(res.rows.item(0).flextype == 1) {
				docBoxUpdated.classList.add("is-checked");
			} else {
				docBoxUpdated.classList.remove("is-checked");
			}
		});
	});
	
	var dialog = document.querySelector('#edit-dialog');
	dialog.showModal();

}
function showDeleteNote(id) {
	var idNote = document.getElementById('numeroNoteDeleted');
	idNote.value = id;
	
	var dialog = document.querySelector('#delete-dialog');
	dialog.showModal();
}

function deleteNote() {
	var id = document.getElementById('numeroNoteDeleted').value;
	var db = window.sqlitePlugin.openDatabase({name: 'krafting_db.db', location: 'default' }, function (db) {}, function (error) { console.log('Open database ERROR: ' + JSON.stringify(error)); });  
	db.transaction(function(tx) {
		var query = "DELETE FROM NOTESUPDATE WHERE id=" + id;
		tx.executeSql(query, [], function(tx, res) {});
	});
	fetchNewData();
	var dialog = document.querySelector('#delete-dialog');
	dialog.close();
	sendChip("alertMessage","contentAlertMessage", "Note supprimée avec succès.");
}