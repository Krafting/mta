# Krafting Mobile Tools App

```cordova run android && adb shell logcat -c```

# Known issues

* La fonction vibrate ne fonctionne pas tant que l'utilisateur ne touche pas l'ecran (donc, ca ne vibre pas dans le jeu en ShakeStyle)
* Quand on ouvre l'iFrame ca casse cordova dans le chat

# Things to do

* Save received note in chat

# Idées Futures

* Traductions en plusieurs Langues
* Refaire les points de la map en localstorage (ou autre, ex: fichier JSON)
* Setup un Server PeerJS sur mon site & tester avec (Tried, but didn't work)

# Done!

* Les Dés!
* Le Clicker!
* Les notes (Creation, Modification, Suppression)
* Systeme de Message en peer-To-Peer
* Generer un QR code (Chat room)
* Va falloir reduire la taille du bouton pour clear les MSG
* Le contrast du text dans le header de la chatroom laisse a désirer
* Bouton Envoyer ma localisation par message
* Le click sur le bouton pour ajouer des notes affiche encore le rose
* Ameliorer la page pour join une room
* Quand on scan un QR Code, le bouton pour join reste coincé dans l'annimation si on cancel le QR
* Afficher un message quand la map n'est pas chargée
* Settings
    * Parametre pour desactiver la vibration quand on recois un message (finalement, desactive toute les vibrations)
    * Parametre pour effacer la BDD du clicker
* Possibilités d'envoyer des images!
* Ne mettre que 1 popup dont le contenu change au lieu de 10 mille popup
* Ameliorer le PopUp des images
* On peut envoyer n'importe quoi avec les images (verifier l'extension)
* En mode Landscape, le POPUP des notes est pas très jolie
* regarde les consignes du truc (Y'a pas grand chose, full Java, donc bof)
* Quand on envoie la location par message, Ajouter un lien qui va sur la map et met un point au coordonnées
* Envoie d'images via la camera, remplacer le bouton envoie d'image par un + qui eten un menu, avec un bouton uploader et un buton prendre une photo
* Corriger le bug des boutons qui sont SOUS l'image
* Possibilité d'ajouter des points sur la map (Localstorage avec Tableaux ?) => Done with IndexedDB
* Pouvoir prendre des photos puis les envoyer
* Les photos prises, sont rotated
* Bulle de texte quand y'a des Event dans le chat 
* Message quand y'a aucune notes
* Page About sur la page d'acccueil au lieu des notifications
* Clean-Up les menu au trois points
* Redesigner l'horodatage des messages, comme sur signal
* Markdown dans les notes
* Ajouter une Options pour activer ou desactiver la sauvegarde des photos qu'on prend
* Reprendre le meme Peer, Mieux: Setup un pseudo dans les settings
* Popup pour valider l'effacement des message dans le chat
* Redesigner les popup de suppressions (Messages & Notes)
* mauvais z-index pour les bulle dans les message (on les vois pas quand y'a le truc des images ouvert)
* Ajouter des shortcuts sur la Homepage via une BDD (Tableau JSON plutot) de tout les liens possibles
* Revoir le Clicker, Ajouter un Hold Me Button
* Ajouter la page Weather
* Redesign Les notes
* Added Markdown Editor
* Added Location on notes
* Option to reset chat username
* Redesign Button Map
* Button "meteo via nom de la ville"
* Ajout d'un bouton "Me localiser" sur la map
* Ajout d'un bouton "Raffraichir la map" sur la map
* Changer la librarie de l'editeur Markdown
* Utiliser la nouvelle librarie pour afficher les notes
* Better CSS Markdown dans les Notes
* Fixed: Un bug: On peut pas edit des notes après un certain temps (Je sais pas comment ce bug fonctionne ???)
* Changer le font pour Ubuntu (ou meilleur) (Pas fais, parce que pas besoin finalement)
* Afficher la vitesse a la quel on vas, avec max
* Créer le bouton pour suivre la location & arrêter de la suivre
* Après avoir cliqué sur un lien de MAP, une erreur apparrait pour prendre des photos (Probleme avec l'iframe) CE BUG S'EST CORRIGÉ TOUT SEUL, INCROYABBBLLLEE
* Revamped text Input design
* Revamped the Holding function on the clicker Button
* Added the Reflexe Game
* Added a "Autre" page to the jeux.html
* Added sound plugin to play sounds
* Finalement, changed some fonts to Ubuntu in some place
* CSS Tweaks
* Fixed Bug: High Score weren't sorted correctly as they aren't stored as INT
* Added the Shake Game Mode
* Changed requirement of seconds
* Fixed the Create Note Bug
* Fixed the display des secondes dans le Reflexe Game
* Added a Compass to the map
* Added the ability to follow point with compass (Default: Anterrieux)
* Added the daily and hourly Forecast in the Weather Page
* Added a gesture library that (somewhat) works
* Added the View weather link in send Location of chat app
* Fixed the quote bug in notes (again)
* Revamped the High-score page on the Reflexes Game
* Show a loading message when loading notes
* Added gestures on all page (except Map for obvious reason)
* House-Keeping in the <head>
* Marco Cisco Papier
* Retour a la ligne quand on ecrit un message (textarea au lieu d'input?)
* Ajouter un text-shadow au icons des photos dans le chat
* Ability to send Notes via messages
* Envoyer les notes par message
* Combined some JS into one file instead of having it twice in send.html and receive.html
* Chips instead of Alert in the note feedback
* Added the Ability to send notes via message (page send.html)
* Fixed setting Page
* Added innerHTML change on holding the Clicker Button
* Separate JS in the weather.html page
* Full color redesign
* Added usefullness to Tools
* Fixed stuff in notes
* Settings for a custom PeerJS server
* Idée: Jeu: page diviser en deux, un point qui apparait sur chaque bloque de la page, premier utilisateur a cliquer le point gagne
* Reworked the Two-sides games page
* change some js folders
* Added end page & timeout dans Two-Sides
* Fixed stuff (setting not displaying settings)
* Option to put custom PeerJS server (goes with creating a custom PeerJS server)
* Les settings PeerJS sont désormais utiliser (A tester avec un server PeerJS dans le futur par contre)
* Updated platforms & plugins
* Added the Fingerprint/Auth plugins
* Added settings for the plugins (does nothins ATM)
* Auth Settings does stuff now
* Simplified some settings function
* Pages udilise the Auth system now
* Dark mode in notes!! (Messages, notes & Editor)
* Moved some files around
* Fixed stuff + New Build!
* using vars in CSS
* Map in one JS file
* Better follow position function
* Cleanup files
* New speedometer in Map & Dark Themed the compass
* Moved files around
* Added Light Theme
* Added setting to switch theme
* Did some tweaks to the light theme & moved stuff around
* Fixed Buttons in games
* Added custom API keys settings (not doing anything yet)
* Option to view API keys
* Custom API keys can now be used!
* Changed colors in the speedometer
* Added the Hangman
* begin work on Juste Prix
* Added the juste-prix
* Added La boule magique + new Build
* New Boule: Shaking, Colors, And more sentences
* Theme Fix & New Hidden Theme
* Impossible de créer la fonction pour télécharger les images
    * Bon, j'ai réussi, après avoir bosser sur une autre application, comme quoi, avoir plusieurs projet en même temps ca a du bon!
* CSS Fix